#GiveYouAHead 0.2.7.x
---

This is a command line tool which will help students to manage there homework-codes, if they do not want to use GNU-make, cmake, or others.

---

## About this version

This is a special version, because this version is going to be used to take part in the Xinghuobei.

And about GiveYouAHead, I totally decided to stop develop this application. Because I think that those make tools can do what this can do, and there is no need to create a make-tool. So the version of 0.3.x or higher will be the history and nothing.

But this version will be upload to Hackage. And it might be upload to XHSK-Hackage, if possible.

---


## About this branch

This branch's main repo is not on the GitHub, but the BitBucket. The reason is that we will build a new repo just for the Xinghuobei, where this repo needs some documents and other things.
